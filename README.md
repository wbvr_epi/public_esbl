# Multidirectional dynamic risk assessment for ESBL source attribution

[![DOI](https://zenodo.org/badge/DOI/10.1016/j.mran.2022.100230.svg)](https://doi.org/10.1016/j.mran.2022.100230)

## Authors: 

Eduardo de Freitas Costa, Thomas J. Hagenaars, Anita Damme-Korevaar, Michael S. M. Brouwer, and Clazien J. de Vos

## Aims:

The main objective of the project is to model a multidirectional dynamic risk assessment source attribution for ESBL. A risk assessment approach is used to answer the following questions:

+ i) Estimate the number of human ESBL colonization attributed to the livestock sector; 

+ ii) Interventions in the food chain (here: chicken consumption), and in which step interventions contribute most to reducing the number of human colonization; 

+ iii) Contribution of broiler flock farms with high antimicrobial usage to the public health burden of ESBL colonization.

## The paper is available online:


## Instrucitons to reproduce the results of the paper

### Using RStudio:

### 1 Download the files to a local or cloud folder. The best option is to download all files zipped;

### 2 Unzip and open the R project "MADRA.Rproj";

### 3 In R, you can open the file\ 

  + "main.R" \ 
  
### 4 Always load the packages before running the model. The model may take a long time to run;


### 5 Note that two folders were created into the working directory:
  - **Output/chicken**: Stores the results of the baseline (scenario1), uncertainty, what-if analysis simulation, final prevalence, and table 4;
  
  - **Figures/chicken**: Stores the graphs obtained in the paper;
